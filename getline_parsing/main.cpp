#include <string>
#include <iostream>

int readIntegerWithStdGetline(std::istream& inputStream, std::ostream& outputErrorStream)
{
	std::string currentLine;
	int integerValue;

	bool wasExceptionThrown = false;
	do
	{
		std::cout << "Enter number: ";
		std::getline(inputStream, currentLine);
		try
		{
			integerValue = std::stoi(currentLine);
			wasExceptionThrown = false;
		}
		catch (const std::invalid_argument& e)
		{
			wasExceptionThrown = true;
			outputErrorStream << e.what() << '\n';
		}
		catch (const std::out_of_range& e)
		{
			wasExceptionThrown = true;
			outputErrorStream << e.what() << '\n';
		}
	} while (wasExceptionThrown);

	return integerValue;
}



int main()
{
	int integer = readIntegerWithStdGetline(std::cin, std::cerr);
	std::cout << "You've written " << integer << ".\n";
	return 0;
}