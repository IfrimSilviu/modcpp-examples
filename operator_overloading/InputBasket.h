#pragma

#include "Item.h"

#include <vector>

class InputBasket
{
public:
    InputBasket(const std::initializer_list<Item>& items);

    InputBasket& operator += (const Item& item);

    friend std::ostream& operator << (std::ostream& outputStream, const InputBasket& inputBasket);

private:
    std::vector<Item> m_Items;
};
