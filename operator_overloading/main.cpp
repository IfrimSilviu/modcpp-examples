#include <iostream>

#include "InputBasket.h"

int main()
{
    Item item1(1, "Cherry");
    Item item2(2, "Strawberry");

    InputBasket inputBasket({item1, item2});
    std::cout<<inputBasket;


    Item item3(3, "Apple");

    // One could add an item by using the operator
    inputBasket += item3;

    std::cout << "-----------------------------------\n" << inputBasket;


    //Or by chaining them
    ((inputBasket += item3) += item2) += item1;

    std::cout << "-----------------------------------\n" << inputBasket;


    return 0;
}