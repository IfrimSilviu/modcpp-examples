#include <cstdint>
#include <string>

class Item
{
public:
    Item(int32_t id, const std::string& name);

    const std::string& getName() const;
    const int32_t getId() const;

private:
    int32_t m_Id;
    std::string m_Name;
};
