#include "Item.h"

Item::Item(int32_t id, const std::string& name) :
    m_Id(id),
    m_Name(name)
{}

const std::string& Item::getName() const
{
    return m_Name;
}

const int32_t Item::getId() const
{
    return m_Id;
}
