#include "InputBasket.h"

#include <algorithm>
#include <ostream>

InputBasket::InputBasket(const std::initializer_list<Item>& items) :
    m_Items(items)
{
}

InputBasket& InputBasket::operator+=(const Item& item)
{
    m_Items.push_back(item);
    return *this;
}


std::ostream& operator<<(std::ostream& outputStream, const InputBasket& inputBasket)
{
    std::for_each(std::cbegin(inputBasket.m_Items), std::cend(inputBasket.m_Items), [&outputStream](const Item& currentItem) { outputStream << currentItem.getId() << " " << currentItem.getName() << "\n"; });
    return outputStream;
}
