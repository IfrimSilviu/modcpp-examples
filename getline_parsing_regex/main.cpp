#include <string>
#include <iostream>
#include <regex>

int readIntegerWithStdGetline(std::istream& inputStream, std::ostream& outputErrorStream)
{
	static const std::regex integerRegex("^(\\+|-)?([1-9]{1}[0-9]*)$");
	std::string currentLine;
	int integerValue;
	bool isInputProper = false;
	do
	{
		std::cout << "Enter number: ";
		std::getline(inputStream, currentLine);

		if (std::regex_match(currentLine, integerRegex))
		{
			integerValue = std::stoi(currentLine); // may throw out of range
			isInputProper = true;
		}
	} while (!isInputProper);

	return integerValue;
}



int main()
{
	int integer = readIntegerWithStdGetline(std::cin, std::cerr);
	std::cout << "You've written " << integer << ".\n";
	return 0;
}